from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import account
import shelve
import random
import string


@csrf_exempt
def show_all_accounts(request):

    account_handler = account.Account()

    customer_id = request.POST.get("customer_id")
    all_accounts = account_handler.get_all_accounts(customer_id)

    info = []
    for acc in all_accounts:
        a = {}
        a['nickname'] = acc['nickname']
        a['_id'] = acc['_id']
        a['type'] = acc['type']
        info.append(a)
    
    return JsonResponse(info, safe=False)


@csrf_exempt
def generate_coupon(request):

    account_handler = account.Account()

    account_id = request.POST.get("account_id")
    amount = float(request.POST.get("amount"))

    this_account = account_handler.get_account(account_id)
    balance = this_account['balance']

    info = {}

    if balance < amount:
        info['Result'] = "Failure"
        info['Reason'] = "Not Enough Funds" 
        return JsonResponse(info)
    
    else:
        info['Result'] = "Success"
        chars=string.ascii_uppercase + string.digits
        code = ''.join(random.choice(chars) for _ in range(10))
        info['Code'] = code

        s = shelve.open('store.db')
        MBAID = s['MBAID']
        result = account_handler.transfer(account_id, MBAID, amount, "ESCROW")
        if result == "Success":
            keys = s.get('CODES', {})
            keys[code] = amount
            s['CODES'] = keys    
            s.close()
            return JsonResponse(info)
        else:
            info['Result'] = "Failure"
            info['Reason'] = "Unknown" 
            s.close()
            return JsonResponse(info)


@csrf_exempt
def redeem_coupon(request):

    account_handler = account.Account()

    account_id = request.POST.get("account_id")
    coupon = request.POST.get("coupon")
    price = float(request.POST.get("price"))

    info = {}
    s = shelve.open('store.db')
    keys = s.get('CODES', {})
    if coupon in keys:
        MBAID = s['MBAID']
        amount = keys[coupon]
        if amount < price:
            info['Result'] = "Failure"
            info['Reason'] = "Not Enough Funds" 
            s.close()
            return JsonResponse(info)
        result = account_handler.transfer(MBAID, account_id, price, "PAYMENT")
        if result == "Success":
            if amount - price > 0:
                keys[coupon] = amount-price
            else:
                del(keys[coupon])
            s['CODES'] = keys
            s.close()
            info['Result'] = "Success"
            return JsonResponse(info)
        else:
            s.close()
            info['Result'] = "Failure"
            info['Reason'] = "Unknown" 
            return JsonResponse(info)
    else:
        s.close()
        info['Result'] = "Failure"
        info['Reason'] = "Invalid Coupon" 
        return JsonResponse(info)


