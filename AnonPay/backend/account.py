from mongoengine import *
import requests
import json

class Account():


    def __init__(self):
        self.apiKey = '80dd2b85735408fa4392f4a6fc14a3b8'
        self.all_accounts_url = 'http://api.reimaginebanking.com/customers/{}/accounts?key={}'
        self.all_customers_url = 'http://api.reimaginebanking.com/customers?key={}'
        self.account_url = 'http://api.reimaginebanking.com/accounts/{}?key={}'
        self.transfer_url = 'http://api.reimaginebanking.com/accounts/{}/transfers?key={}'

    def get_account(self, account_id):
        url = self.account_url.format(account_id, self.apiKey)

        response = requests.get(url)
        json_data = json.loads(response.text)
        return json_data

    def get_all_accounts(self, customer_id):
        url = self.all_accounts_url.format(customer_id, self.apiKey)

        response = requests.get(url)
        json_data = json.loads(response.text)
        return json_data

    def get_all_customers(self, customer_id):
        url = self.all_customers_url.format(self.apiKey)

        response = requests.get(url)
        json_data = json.loads(response.text)
        return json_data

    def transfer(self, source, destination, amount, desc):
        url = self.transfer_url.format(source,self.apiKey)
        payload = {
                "medium": "balance",
                "payee_id": destination,
                "amount": amount,
                "transaction_date": "2016-01-24",
                "status": "completed",
                "description": desc
                }
        response = requests.post(url, data=json.dumps(payload),headers={'content-type':'application/json'})
        print response.text

        if response.status_code == 201:
            return "Success"
        else:
            return "Failure"

