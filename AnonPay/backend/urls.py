from django.conf.urls import url
from . import views

urlpatterns = [
        url(r'^generate_coupon$', views.generate_coupon, name='generate_coupon'),
        url(r'^show_all_accounts$', views.show_all_accounts, name='show_all_accounts'),
        url(r'^redeem_coupon$', views.redeem_coupon, name='redeem_coupon'),
        ]
