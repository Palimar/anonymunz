(function() {
    // Localize jQuery variable
    var jQuery;

    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.12.0') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute("src",
                                "http://code.jquery.com/jquery-1.12.0.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else { // Other browsers
            script_tag.onload = scriptLoadHandler;
        }
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function
        main(); 
    }

    function main() { 
        jQuery(document).ready(function($) {
            var $button = jQuery('<img src="assets/anonymous-129.png" alt="Check out with PayPal" />');
            $button.click(function() {
                window.open('http://www.cnn.com/', 'Anonymau5_Window', 'menubar=0 toolbar=0');
            });
            jQuery("#anonymau5-widget-container").append($button);
        });
    }

})(); // We call our anonymous function immediately
